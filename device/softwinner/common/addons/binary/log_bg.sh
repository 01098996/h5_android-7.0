#!/system/bin/sh


let "size = 20"
let "count = 0"

for  i in `ls /data/anr/aw_*.log`
do
    let "count+=1"
done
let "count = count/2"

if [ "$count" -ge "$size" ]; then
let "count = 0"
while [ "$size" -gt "$count" ];
do
    mv /data/anr/aw_${count}_system_boot.log /data/local/tmp/old_aw_${count}_system_boot.log
    mv /data/anr/aw_${count}_kernel_boot.log /data/local/tmp/old_aw_${count}_kernel_boot.log
    let "count+=1"
done
fi

let "count = 0"
for  i in `ls /data/anr/aw_*.log`
do
    let "count+=1"
done
let "count = count/2"

cat /proc/kmsg >> /data/anr/aw_${count}_kernel_boot.log &
sleep 4
logcat -v time >> /data/anr/aw_${count}_system_boot.log &
wait


