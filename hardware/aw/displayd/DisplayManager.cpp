/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "helper.h"
#include "DisplayManager.h"
#include <cutils/log.h>

DisplayManager *DisplayManager::instance = new DisplayManager();

DisplayManager *
DisplayManager::getInstance() {
	return instance;
}

DisplayManager::DisplayManager() {
	mProxy = new HWC1Proxy();
	mControler = new deviceControler();
}

deviceControler*
DisplayManager::getControler() {
	return mControler;
}

int
DisplayManager::listInterface(int display, std::vector<int> *interfaces) {
	interfaces->clear();
	int type = mProxy->getOutputType(display);
	interfaces->push_back(type);
	return 0;
}

int
DisplayManager::getCurrentInterface(int display, int *interface) {
	*interface = mProxy->getOutputType(display);
	return 0;
}

int
DisplayManager::setCurrentInterface(int display, int interface, int enable) {

	int current = mProxy->getOutputType(display);
	if ((current != interface) && current != DISP_OUTPUT_TYPE_NONE) {
		ALOGE("display %d not support type %d", display, interface);
		return -1;
	}
	if (enable)
		mProxy->setOutputMode(display, interface, 0xff);
	else
		mProxy->setOutputMode(display, 0, 0);

	return 0;
}

int
DisplayManager::listMode(int display, int interface, std::vector<int> *modes) {
	if (mProxy->getOutputType(display) != interface) {
		ALOGE("display %d not support type %d", display, interface);
		return -1;
	}

	if (interface == DISP_OUTPUT_TYPE_HDMI) {
		std::vector<int> maximum;
		getHdmiModes(&maximum);
		for (int m : maximum) {
			if (isSupportMode(display, m) == 1)
				modes->push_back(m);
		}
	} else if (interface == DISP_OUTPUT_TYPE_CVBS) {
		getCvbsModes(modes);
	}
	return 0;
}

int
DisplayManager::getCurrentMode(int display, int interface, int *mode) {
	if (mProxy->getOutputType(display) != interface) {
		ALOGE("display %d not support type %d", display, interface);
		return -1;
	}
	*mode = mProxy->getOutputMode(display);
	return 0;
}

int
DisplayManager::setCurrentMode(int display, int interface, int mode) {
	if (mProxy->getOutputType(display) != interface) {
		ALOGE("display %d not support type %d", display, interface);
		return -1;
	}
	if (interface == DISP_OUTPUT_TYPE_HDMI && isSupportMode(display, mode) != 1) {
		ALOGE("display %d not support hdmi mode %d", display, mode);
		return -1;
	}
	mProxy->setOutputMode(display, interface, mode);
	return 0;
}

int
DisplayManager::setCurrentMode_FORCE(int display, int interface, int mode) {
	ALOGD("Force: display %d type %d mode %d", display, interface, mode);
	mProxy->setOutputMode(display, interface, mode);
	return 0;
}

int
DisplayManager::getOutputFormat(int display) {
	int type, mode;
	if (mProxy->getOutputFormat(display, &type, &mode) == 0)
		return ((type << 8) | mode);
	return 0;
}

int
DisplayManager::setOutputFormat(int display, int format) {
	int type = (format >> 8) & 0xff;
	int mode = (format >> 0) & 0xff;

	if (mProxy->getOutputType(display) != type) {
		ALOGE("display %d not support type %d", display, type);
		return -1;
	}
	if (isSupportMode(display, mode) != 1) {
		ALOGE("display %d not support mode %d", display, mode);
		return -1;
	}
	return mProxy->setOutputMode(display, type, mode);
}

int
DisplayManager::getOutputType(int display) {
	return mProxy->getOutputType(display);
}

int
DisplayManager::getOutputMode(int display) {
	return mProxy->getOutputMode(display);
}

int
DisplayManager::setOutputMode(int display, int mode) {

	if (isSupportMode(display, mode) != 1) {
		ALOGE("display %d not support mode %d", display, mode);
		return -1;
	}

	int type = mProxy->getOutputType(display);
	return mProxy->setOutputMode(display, type, mode);
}

int
DisplayManager::isSupportMode(int display, int mode) {
	int type = mProxy->getOutputType(display);
	if (type == DISP_OUTPUT_TYPE_CVBS)
		return (mode == DISP_TV_MOD_PAL) || (mode == DISP_TV_MOD_NTSC);

	if (type == DISP_OUTPUT_TYPE_HDMI)
		return mControler->isSupportHdmiMode(display, mode);
	return 0;
}

int
DisplayManager::listSupportModes(int display, std::vector<int> *modes) {
	int type = mProxy->getOutputType(display);
	if (type == DISP_OUTPUT_TYPE_HDMI) {
		std::vector<int> maximum;
		getHdmiModes(&maximum);
		for (int m : maximum) {
			if (isSupportMode(display, m) == 1)
				modes->push_back(m);
		}
	} else if (type == DISP_OUTPUT_TYPE_CVBS) {
		getCvbsModes(modes);
	}
	return 0;
}

int
DisplayManager::isSupport3DMode(int display, int mode) {
	if (getOutputType(display) != DISP_OUTPUT_TYPE_HDMI)
		return 0;
	if (mode == -1)
		mode = DISP_TV_MOD_1080P_24HZ_3D_FP;
	return mControler->isSupport3DMode(display, mode);
}

int
DisplayManager::getDisplayMargin(int display, int *margin_x, int *margin_y) {
	return mProxy->getScreenMargin(display, margin_x, margin_y);
}

int
DisplayManager::setDisplayMargin(int display, int margin_x, int margin_y) {
	return mProxy->setScreenMargin(display, margin_x, margin_y);
}

int
DisplayManager::getDisplayOffset(int display, int *offset_x, int *offset_y) {
	return mProxy->getScreenOffset(display, offset_x, offset_y);
}

int
DisplayManager::setDisplayOffset(int display, int offset_x, int offset_y) {
	return mProxy->setScreenOffset(display, offset_x, offset_y);
}

int
DisplayManager::set3DLayerMode(int display, int mode) {
	return mProxy->set3DLayerMode(display, mode);
}
