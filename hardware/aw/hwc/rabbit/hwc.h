/*
 * Copyright (C) 2007-2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __HWCOMPOSER_PRIV_H__
#define __HWCOMPOSER_PRIV_H__

#include <hardware/hardware.h>
#include <hardware/hwcomposer.h>

#include <hardware/hal_public.h>
#include "sunxi_display2.h"
//#include <fb.h>

#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <cutils/log.h>
#include <cutils/atomic.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <poll.h>
#include <cutils/properties.h>
#include <hardware_legacy/uevent.h>
#include <sys/resource.h>
#include <EGL/egl.h>
#include <linux/ion.h>
#include <ion/ion.h>
#include <sys/ioctl.h>
#include <math.h>
#include <sys/mman.h>
#include <utils/Trace.h>
#include <pthread.h>
#include <edid_parse/parse_edid_sunxi.h>

#define HAL_PIXEL_FORMAT_BGRX_8888  0x1ff
#define ION_IOC_SUNXI_PHYS_ADDR     7
#define ALIGN(x,a)  (((x) + (a) - 1L) & ~((a) - 1L))
#define HW_ALIGN    64
#define YV12_ALIGN 16
#define VE_YV12_ALIGN 16
#define DISPLAY_MAX_LAYER_NUM 16
#define HOLD_BUFS_UNTIL_NOT_DISPLAYED
#define HAS_HDCP 1
//#define SYSRSL_SWITCH_ON_EX

#define NUMLAYEROFCHANNEL 4
#define NUMCHANNELOFVIDEO 1
#define NUMCHANNELOFDSP   4
// tmp open 2 channel
#define PVRSRV_MEM_CONTIGUOUS               (1U<<15)

enum {
    HW_CHANNEL_PRIMARY = 0,
    HW_CHANNEL_EXTERNAL,
    NUMBEROFDISPLAY,
    F_HWDISP_ID,
    EVALID_HWDISP_ID,
};

typedef enum {
    ASSIGN_INIT = 0,
    ASSIGN_GPU = 1,
    ASSIGN_OVERLAY = 2,
    ASSIGN_NEEDREASSIGNED = 3,
    ASSIGN_FAILED = 4,
    ASSIGN_CURSOR_OVERLAY = 5
} HwcAssignStatus;


typedef enum {
#define AssignDUETO(x) x,
    AssignDUETO(I_OVERLAY)
    AssignDUETO(D_NULL_BUF)
    AssignDUETO(D_CONTIG_MEM)
    AssignDUETO(D_VIDEO_PD)
    AssignDUETO(D_CANNT_SCALE)
    AssignDUETO(D_SKIP_LAYER)
    AssignDUETO(D_NO_FORMAT)
    AssignDUETO(D_BACKGROUND)
    AssignDUETO(D_TR_N_0)
    AssignDUETO(D_ALPHA)
    AssignDUETO(D_X_FB)
    AssignDUETO(D_SW_OFTEN)
    AssignDUETO(D_SCALE_OUT)
    AssignDUETO(D_STOP_HWC)
    AssignDUETO(D_NO_PIPE)
    AssignDUETO(D_MEM_LIMIT)
#undef AssignDUETO
    DEFAULT,

} AssignDUETO_T;

enum {
    FPS_SHOW = 1,
    LAYER_DUMP = 2,
    SHOW_ALL = 3
};

enum {
    DISP_NONE_POLICY = 0,
    DISP_SINGLE_POLICY,
    DISP_DUAL_POLICY,
    DISP_ADAPT_POLICY,
    DISP_POLICY_NUM,
};

typedef struct {
    int speed; // ddr freq
    int limit;
}mem_speed_limit_t;

typedef struct bandwidth_record {
    int max;
    int available;
    int video;
    int fb;
    int total;
}bandwidth_t;

typedef struct {

    int                     outaquirefencefd;
    int                     width;
    int                     Wstride;
    int                     height;
    disp_pixel_format       format;
    unsigned int            phys_addr;

}WriteBack_t;

typedef struct ion_list {
    int                 has_ref;
    unsigned int        frame_index;
    int                 fd;
    ion_user_handle_t   handle;
    struct ion_list     *prev;
    struct ion_list     *next;
}ion_list_t;

typedef struct {
    int               deviceid;
    int               layer_num;
    disp_layer_config *hwclayer;
    int               *fencefd;
    bool              needWB;
    unsigned int      ehancemode;
    unsigned int      androidfrmnum;
    WriteBack_t       *WriteBackdata;
} setup_dispc_data_t;

typedef struct layer_info {
    HwcAssignStatus assigned;
    signed char     hwchannel;
    signed char     virchannel;
    signed char     HwzOrder;
    signed char     OrigOrder;
    bool            is3D;
    AssignDUETO_T   info;
    hwc_layer_1_t   *psLayer;
    int             bandwidth;
}layer_info_t;

typedef struct ChannelInfo {
    bool            hasBlend;
    bool            hasVideo;
    bool            isFB;
    float           WTScaleFactor;
    float           HTScaleFactor;
    int             iCHFormat;
    unsigned char   planeAlpha;
    int             HwLayerCnt;//0~n ,0 is the first,  current will used
    layer_info_t   *HwLayer[4];
} ChannelInfo_t;

typedef struct {
    int                 VirtualToHWDisplay;
    bool                setblank;
    bool                VsyncEnable; //fixme: should check after exchangeDispChannel
    bool                issecure;
    unsigned int        ehancemode;

    int                 HwChannelNum;
    int                 LayerNumofCH;
    int                 VideoCHNum;

    int                 InitDisplayWidth;
    int                 InitDisplayHeight;
    int                 VarDisplayWidth;
    int                 VarDisplayHeight;
    uint64_t            mytimestamp;
    unsigned int        DiplayDPI_X;
    unsigned int        DiplayDPI_Y;
    unsigned int        DisplayVsyncP;
    int                 DisplayFps;

    unsigned char       SetPersentWidth;
    unsigned char       SetPersentHeight;
    unsigned char       PersentWidth;
    unsigned char       PersentHeight;

    int                 DisplayType;
    int                 DisplayMode;
    __display_3d_mode   Current3DMode;
    int                 setDispMode; //for saving displayMode when 3d display

    long long           fps;
    long long           de_clk;
    int                 screenRadio;
    bandwidth_t         bandwidth;
}DisplayInfo;

typedef struct {
    bool                UsedFB;
    unsigned char       FBhasVideo;
    signed char         HwCHUsedCnt;//0~n ,0 is the first,  current is used
    signed char         VideoCHCnt;//0~n, 0 is the first,  current is used
    unsigned char       HaveVideo;
    unsigned char       haveAlpha;
    float               WidthScaleFactor;
    float               HighetScaleFactor;

    layer_info         *psAllLayer;
    unsigned int allocNumLayer;
    int                 numberofLayer;
    const DisplayInfo  *psDisplayInfo;
    ChannelInfo_t       ChannelInfo[4];  //zOrder 0~3

}HwcDisContext_t;

typedef struct {
    hwc_procs_t const*  psHwcProcs;
    pthread_t           sVsyncThread;
    int                 DisplayFd;
    int                 FBFd;
    int                 IonFd;
    int                 dvfsfd;

    unsigned int        HWCFramecount;

    bool                CanForceGPUCom;
    bool                ForceGPUComp;
    bool                DetectError;
    char                hwcdebug;

    unsigned int        uiBeginFrame;
    double              fBeginTime;
    int                 exchangeDispChannel;
    pthread_mutex_t     lock; //for exchangeDispChannel

    int                 NumberofDisp;
    DisplayInfo         *SunxiDisplay;
    HwcDisContext_t     *DisContext;
    setup_dispc_data_t  PrivateData[2];
    ion_list            *IonHandleHead;
    ion_list            *ionCurValidNode; // point the that has not ref of buf
    int                 ionListCurNum;
    int                 ionListTotalNum;

    int                 ban4k;
    int                 dispPolicy;
    bool                blank;
}SUNXI_hwcdev_context_t;

typedef struct {
    int             type;// bit3:cvbs, bit2:ypbpr, bit1:vga, bit0:hdmi
    int             mode;
    int             width;
    int             height;
    int             refreshRate;
    int             support;
}tv_para_t;

typedef enum {
    WIDTH=2,
    HEIGHT,
    REFRESHRAE,

}MODEINFO;

typedef enum {
    FIND_HWDISPNUM=0,
    FIND_HWTYPE,
    NULL_DISPLAY,
    SET_DISP,
    FREE_DISP,

}ManageDisp;

typedef struct {
    ion_user_handle_t handle;
    unsigned int phys_addr;
    unsigned int size;
}sunxi_phys_data;

typedef struct {
    long    start;
    long    end;
}sunxi_cache_range;

#define ION_IOC_SUNXI_FLUSH_RANGE  5


extern SUNXI_hwcdev_context_t gSunxiHwcDevice;

extern int hwcdev_reset_device(SUNXI_hwcdev_context_t *psDevice, size_t disp);
extern HwcAssignStatus hwc_try_assign_layer(HwcDisContext_t *ctx, size_t  singcout,int zOrder);
extern SUNXI_hwcdev_context_t* hwc_create_device(void);
extern int  _hwcdev_layer_config_3d(const DisplayInfo  *PsDisplayInfo, disp_layer_info *layer_info);
extern disp_tv_mode get_suitable_hdmi_mode(int i);
extern void *VsyncThreadWrapper(void *priv);
extern int hwc_setup_layer(HwcDisContext_t *ctx);
extern int hwc_reset_disp(SUNXI_hwcdev_context_t *ctx);
extern int _hwc_device_set_3d_mode(int disp, __display_3d_mode mode);
extern int _hwc_device_set_backlight(int disp, int on_off,bool half);
extern int _hwc_device_set_enhancemode(int disp, bool on_off,bool half);
extern int hwc_region_intersect(hwc_rect_t *rect0, hwc_rect_t *rect1);
extern int hwc_destroy_device(void);
SUNXI_hwcdev_context_t* hwc_create_device(void);
extern int get_info_mode(int mode,MODEINFO info);
extern int getVgaInfo(int mode,MODEINFO info);
int getMemLimit();
int getValueFromProperty(char const* propName);
int isDisplayP2P(void);
int getTvMode4SysResolution(void);
int getDispPolicy(void);
int getDispModeFromFile(int type);
int saveDispModeToFile(int type, int mode);
int getDispMarginFromFile(unsigned char * percentWidth,unsigned char * percentHeight);
int saveDispMarginToFile(unsigned char percentWidth,unsigned char percentHeight);
unsigned int isHdmiHpd(int disp);
unsigned int isCvbsHpd(int disp);
extern unsigned int get_ion_addr(int fd);
extern int hwcOutputSwitch(DisplayInfo * psDisplayInfo,int type,int mode);
extern int hwcOutputExchange();
extern int getHwDispByType(int type);
int get_de_freq_and_fps(DisplayInfo *psDisplayInfo);
extern int resetDispMode(int disp, int type,int mode);
extern int check4KBan(void);
int _hwc_device_set_output_mode(int disp, int out_type, int out_mode);
int IonHandleDecRef(char dec_all);
bool sunxi_prepare(hwc_display_contents_1_t **displays ,size_t NumofDisp);
bool sunxi_set(hwc_display_contents_1_t** displays, size_t numDisplays,
    int *releasefencefd, int *retirefencefd);
int initDisplayDevice(int disp,int hwDisp,disp_output * dispOutput);

int _hwc_device_set_margin(int disp,int hpercent,int vpercent);
int  _hwc_set_persent(int disp,int para0, int para1);
int _hwc_device_is_support_hdmi_mode(int disp,int mode);
int _hwc_device_get_output_type(int disp);
int _hwc_device_get_output_mode(int disp);
int _hwc_device_set_screenradio(int disp, int screen_radio);
int is3DMode(__display_3d_mode mode);

#endif
