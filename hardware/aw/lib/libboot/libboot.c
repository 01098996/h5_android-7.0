/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libboot.h"
#include "sunxi_boot_api.h"
#include "private_boot0.h"
#include "private_uboot.h"

static int read_display_parameter(void *buffer, void *param)
{
	boot0_file_head_t *boot0_head = NULL;

	if (!buffer || !param)
		return -1;

	boot0_head = (boot0_file_head_t *)buffer;

	/* read parameter value below */
	struct user_display_param *out = (struct user_display_param *)param;

	out->disp_mode = boot0_head->prvt_head.disp_mode;

	return 0;
}

static int sync_display_parameter(void *buffer, void *param)
{
	boot0_file_head_t *para_head = NULL;

	if (!buffer || !param)
		return -1;

	para_head = (boot0_file_head_t *)buffer;

	if(strncmp((const char *)para_head->boot_head.magic, BOOT0_MAGIC, MAGIC_SIZE))
	{
		fprintf(stderr, "parameter magic err\n");
		return -1;
	}

	/* modify parameter value below */
	struct user_display_param *in = param;
	para_head->prvt_head.disp_mode = in->disp_mode;

	return 1;
}

int libboot_read_display_param(struct user_display_param *out)
{
	return libboot_read_parameter(read_display_parameter, out);
}

int libboot_sync_display_param(struct user_display_param *in)
{
	return libboot_update_parameter(sync_display_parameter, in);
}
