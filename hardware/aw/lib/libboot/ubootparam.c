/*
 * Sunxi Soc uboot env update utils
 * Base on u-boot/tools/env
 *
 * Copyright (C) 2015-2018 AllwinnerTech, Inc.
 *
 * Contacts:
 * Zeng.Yajian <zengyajian@allwinnertech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include "libboot.h"

static void
usage(FILE *out, const char *name) {
	fprintf(out, "Usage: %s [-h] [-p] [-w name value] [-r name]\n\n", name);
	fprintf(out,
		"       -h, --help  Show this help message\n"
		"       -p, --print Print the whole uboot env\n"
		"       -w, --write Write env 'name' with 'value'\n");
}

const struct option longopts[] = {
	{"help",  no_argument,       0, 'h'},
	{"print", no_argument,       0, 'p'},
	{"write", required_argument, 0, 'w'},
	{0, 0, 0, 0}
};

int print_display_param(void);
int save_display_param(char *name, char *value);

int main (int argc, char **argv)
{
	int ch;
	int help, argout, action;
	char *name = 0;

	action = help = argout = 0;
	while (!argout &&
		(ch = getopt_long(argc, argv, "hpw:", longopts, 0)) != -1) {
		switch (ch) {
		case 'p':
			print_display_param();
			argout = 1;
			break;
		case 'w':
			action = 1;
			name = optarg;
			break;
		case 'h':
		default :
			help = argout = 1;
		}
	}
	if (help) {
		usage(stdout, argv[0]);
		return EXIT_SUCCESS;
	}
	argc -= optind;

	if (action == 1) {
		char *value = (argv+optind)[0];
		char *prev_value = NULL;

		if (argc != 1)
			goto _error;

		/*
		 * read the prev value of the special name,
		 * only update the env when the value is change.
		 */
		struct user_display_param param;
		memset(&param, 0, sizeof(param));
		if (libboot_read_display_param(&param) != 0) {
			fprintf(stderr, "libboot read display params failed\n");
			goto _error;
		}
		if (strcmp(name, "disp_rsl") == 0)
			prev_value = (char *)param.resolution;

		if (strcmp(name, "vendorid") == 0)
			prev_value = (char *)param.vendorid;

		printf("name: %s, prev: %s, current: %s\n", name, prev_value, value);

		if (strcmp(prev_value, value)) {
			printf("do write ubootenv\n");
			save_display_param(name, value);
		}
	}

	return 0;

_error:
	usage(stderr, argv[0]);
	return EXIT_FAILURE;
}

int print_display_param(void)
{
	struct user_display_param param;

	memset(&param, 0, sizeof(param));
	if (libboot_read_display_param(&param) != 0)
		fprintf(stderr, "libboot read display params failed\n");

	fprintf(stdout, "resolution:\n%s\n", param.resolution);
	fprintf(stdout, "margin    :\n%s\n", param.margin);
	fprintf(stdout, "vendorid  :\n%s\n", param.vendorid);
	return 0;
}

int save_display_param(char *name, char *value)
{

	char *prev_value = NULL;
	struct user_display_param param;

	memset(&param, 0, sizeof(param));
	if (libboot_read_display_param(&param) != 0) {
		fprintf(stderr, "libboot read display params failed\n");
		return -1;
	}

	if (strcmp(name, "disp_rsl") == 0) {
		prev_value = (char *)param.resolution;
		strncpy(prev_value, value, sizeof(param.resolution));
	}

	if (strcmp(name, "vendorid") == 0) {
		prev_value = (char *)param.vendorid;
		strncpy(prev_value, value, sizeof(param.vendorid));
	}
	return libboot_sync_display_param(&param);
}

